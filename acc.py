class Account:
    def __init__(self,filepath):
        self.filepath=filepath
        with open(filepath,'r')as file:
            self.balance=int(file.read())

    def withdraw(self,amount):
        self.balance=self.balance-amount

    def deposit(self,amount):
        self.balance=self.balance+amount

    def commit(self):
        with open(self.filepath,'w')as file:
            file.write(str(self.balance))

class Checking(Account):
    """This class generates checking objects"""

    type='checking'
 
    def __init__(self, filepath, fee):
        Account.__init__(self, filepath)
        self.fee=fee
 
    def transfer(self, amount):
        self.balance=self.balance - amount- self.fee
 
 
jones_checking=Checking("account\\jones.txt", 1)
jones_checking.transfer(200)
print(jones_checking.balance)
jones_checking.commit()
print(jones_checking.type)

jacks_checking=Checking("account\\jacks.txt", 1)
jacks_checking.transfer(200)
print(jacks_checking.balance)
jacks_checking.commit()
print(jacks_checking.type)